<?php
/************************************************\
 * File Uploader
 * **********************************************
 * File Name	: common.inc.php
 * Author       : Tuan Do @ www.celerondude.com
 * Email		: dork@celerondude.com
 * Purpose      : common variables
\************************************************/
$users_file         = 'data/users.db';
$settings_file      = 'data/settings.db';
$log_file           = 'data/logs.db';
?>
